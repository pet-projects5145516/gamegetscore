
using Task1;


namespace Tests;

[TestFixture]
public class GetScoreTests
{

    static Score unknown_score = new Score(-1, -1);

    static GameStamp[] stamps =
            {
                        new GameStamp(0,0,0),
                        new GameStamp(11,1,9),
                        new GameStamp(45,2,9),
                        new GameStamp(148,3,91),
                        new GameStamp(1999,5,91),
                    };



    [Test]
    public void EmptyGame()
    {
        Game g = new Game();
        Score score = g.getScore(100);
        Assert.AreEqual(score, unknown_score);
    }

    [Test]
    public void ZeroOffset()
    {
        Game g = new Game(stamps);
        var score = g.getScore(0);
        Assert.AreEqual(score, new Score(0, 0));
    }

    [Test]
    public void OutOfRangeOffset()
    {
        Game g = new Game(stamps);
        Score score;

        score = g.getScore(int.MinValue);
        Assert.AreEqual(score, unknown_score);

        score = g.getScore(int.MaxValue);
        Assert.AreEqual(score, unknown_score);

    }

    [TestCase(11, 1, 9)]
    [TestCase(45, 2, 9)]
    [TestCase(1999, 5, 91)]
    [Test]
    public void StartMiddleEndPositionTest(int offset, int home, int away)
    {
        Game g = new Game(stamps);

				Score score = g.getScore(offset);
        Score exp_score = new Score(home, away);
				Assert.AreEqual(score, exp_score);
    }

    [TestCase(46, -1, -1)]
    [Test]
    public void UnknownScore(int offset, int home, int away)
    {
        Game g = new Game(stamps);

				Score score = g.getScore(offset);
        Score exp_score = new Score(home, away);
				Assert.AreEqual(score, exp_score);
    }

}
